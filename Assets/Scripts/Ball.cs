﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ball : MonoBehaviour {
    private Paddle paddle;
    private Vector3 paddleToBallVector;
    private bool hasStarted = false;
    private int lives = PlayerData.livesCount;
    private LevelManager levelManager;
    private GameObject live;

    // Use this for initialization
    void Start () {
        paddle = GameObject.FindObjectOfType<Paddle>();
        paddleToBallVector = this.transform.position - paddle.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        // Lock the ball relative to the paddle
        if (!hasStarted)
        {
            this.transform.position = paddle.transform.position + paddleToBallVector;
            //Wait to a mouse press to launch
            if (Input.GetMouseButtonDown(0))
            {
                hasStarted = true;
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(2f, 10f);
            }
        }         
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 tweak = new Vector2(Random.Range(0f, 0.2f), Random.Range(0f, 0.2f));

        if (hasStarted)
        {
            GetComponent<AudioSource>().Play();
            GetComponent<Rigidbody2D>().velocity += tweak;
        }        
    }
    private void OnTriggerEnter2D(Collider2D trigger)
    {
        lives--;

        if (lives <= 0)
        {
            Brick.breakableCount = 0;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);            
        }
        else
        {
            live = GameObject.Find("live-" + lives);
            Destroy(live);
            hasStarted = false;
        }
    }
}
