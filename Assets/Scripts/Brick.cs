﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour {
    public AudioClip hit;
    public AudioClip crack;
    public static int breakableCount = 0;
    public Sprite[] hitSprites;
    public GameObject smoke;
    
    private int timesHit;
    private LevelManager levelManager;
    private bool isBreakable;

    // Use this for initialization
    void Start ()
    {       
        isBreakable = (this.tag == "Breakable");

        if (isBreakable)
        {
            breakableCount++;
        }
        
        levelManager = GameObject.FindObjectOfType<LevelManager>();
        timesHit = 0;    
    }
	
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isBreakable)
        {            
            HandleHits();
        }
    }

    void HandleHits ()
    {
        timesHit++;
        int maxHits = hitSprites.Length + 1;

        if (timesHit >= maxHits)
        {
            AudioSource.PlayClipAtPoint(crack, transform.position, 1f);
            breakableCount--;
            levelManager.BrickDestroyed();
            PuffSmoke();
            Destroy(gameObject);
        }
        else
        {
            AudioSource.PlayClipAtPoint(hit, transform.position);
            LoadSprites();
        }
    }

    void PuffSmoke ()
    {
        GameObject smokePuff = Instantiate(smoke, transform.position, Quaternion.identity) as GameObject;
        smokePuff.GetComponent<ParticleSystem>().startColor = gameObject.GetComponent<SpriteRenderer>().color;
    }

    void LoadSprites()
    {
        int spriteIndex = timesHit - 1;

        if (hitSprites[spriteIndex] != null)
        {
            this.GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
        } else
        {
            Debug.LogError("Brick sprite is missing!"); 
        }   
    }
}
